## About

This project implements offline speech recognition for Android using the [vosk](https://github.com/alphacep/vosk-api) API.

## Roadmap (in no particular order)

- [x] speech input support
- [x] multiple languages support
- [ ] multiple models for the same language (with grammars?)
- [x] link the settings activity to the speech input settings (android system settings)
- [ ] make the app build reproducible
- [ ] (maybe) register to handle zip files and decompress the models
- [ ] (maybe) allow to delete installed models
- [x] (maybe) add a speech IME

## Contributing

This is a project I work on in my spare time, for my own use case and with my own schedule and priorities.
Maintaining a community project is a hard work, that I know for having helped in doing it in the past.
I am not looking forward to repeating that experience right now.
**This is the reason of issues and PR being closed in this repository.**


