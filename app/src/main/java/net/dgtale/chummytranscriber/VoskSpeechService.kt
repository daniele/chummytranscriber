package net.dgtale.chummytranscriber

import android.content.Intent
import android.os.Bundle
import android.os.LocaleList
import android.speech.RecognitionService
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import kotlinx.coroutines.*
import org.json.JSONException
import org.json.JSONObject
import org.kaldi.*
import java.io.File


class VoskSpeechService : RecognitionService(), RecognitionListener {

    private val setup = SupervisorJob()
    private val scope = CoroutineScope(Dispatchers.IO + setup)

    private lateinit var speechService: SpeechService
    private lateinit var modelInUse: String
    private var hasUserSpokenYet: Boolean = false
    private var returnPartialResults: Boolean = false

    private lateinit var mCallback: Callback


    override fun onStartListening(intent: Intent, callback: Callback) {
        mCallback = callback

        // get known model paths as configured in shared preferences
        val availableModels = getSharedPreferences("chummytranscriber", MODE_PRIVATE).all.entries.groupBy({ it.value }, { File(getExternalFilesDir(null)?.absolutePath + "/" + it.key) })

        // if there is a model for the requested language, use that; otherwise use the first among the system default languages
        // TODO: should one of the model be the fallback?
        val modelToUse = availableModels.getOrElse(intent.getStringExtra(RecognizerIntent.EXTRA_LANGUAGE)) {
            availableModels.get(LocaleList.getDefault().get(0).language)
        }

        if (modelToUse.isNullOrEmpty()) {
            mCallback.error(SpeechRecognizer.ERROR_CLIENT)
            return
        }

        returnPartialResults = intent.getBooleanExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, false)

//        val bundle: Bundle? = intent.extras
//        if (bundle != null) {
//            for (key in bundle.keySet()) {
//                Log.i("AA", key + " : " + if (bundle[key] != null) bundle[key] else "NULL")
//            }
//        }

        if (::speechService.isInitialized && ::modelInUse.isInitialized && modelInUse == modelToUse[0].absolutePath) {
            //there is no need to re-read the model and rebuild the speechservice, we can reuse the existing one
            hasUserSpokenYet = false
            speechService.startListening()
            mCallback.readyForSpeech(Bundle())
        } else {

            modelInUse = modelToUse[0].absolutePath

            scope.launch {
                Vosk.SetLogLevel(0)
                val model = Model(modelInUse)
                val recognizer = KaldiRecognizer(model, 16000.0f)
                speechService = SpeechService(recognizer, 16000.0f)
                speechService.addListener(this@VoskSpeechService)
                speechService.startListening()
                mCallback.readyForSpeech(Bundle())
            }
        }
    }

    override fun onCancel(callback: Callback?) {
        scope.cancel()
    }

    override fun onStopListening(callback: Callback?) {
        mCallback.endOfSpeech()
    }

    override fun onPartialResult(result: String) {
        if (!hasUserSpokenYet) {
            mCallback.beginningOfSpeech()
            hasUserSpokenYet = true
        } else {
            if (returnPartialResults)
                mCallback.partialResults(createResultsBundle(result, "partial"))
        }
    }

    override fun onResult(result: String) {
        mCallback.results(createResultsBundle(result, "text"))
        speechService.cancel()
    }

    override fun onError(ex: Exception?) {
        scope.cancel()
    }

    override fun onTimeout() {
        TODO("Not yet implemented")
    }


    private fun createResultsBundle(hypothesis: String, key: String): Bundle {
        val hypotheses: ArrayList<String> = ArrayList()
        val bundle = Bundle()

        try {
            hypotheses.add(JSONObject(hypothesis).getString(key))
            bundle.putStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION, hypotheses)
        } catch (e: JSONException) {

        }
        return bundle
    }

}