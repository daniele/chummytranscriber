package net.dgtale.chummytranscriber

import android.content.Intent
import android.graphics.Color
import android.inputmethodservice.InputMethodService
import android.os.Bundle
import android.speech.RecognitionListener
import android.speech.RecognizerIntent
import android.speech.SpeechRecognizer
import android.util.Log
import android.view.View
import android.view.inputmethod.EditorInfo
import android.view.inputmethod.InputConnection
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.ImageButton
import android.widget.TextView


class IMEService : InputMethodService() {

    private lateinit var s: SpeechRecognizer
    private lateinit var micButton: ImageButton

    override fun onDestroy() {
        super.onDestroy()
        s.destroy()
    }

    override fun onCreateInputView(): View {
        Log.i("AA", "onCreateInputView")

        s = SpeechRecognizer.createSpeechRecognizer(this)

        return layoutInflater.inflate(R.layout.chummyime, null).apply {

            val inputMethodManager = getSystemService(INPUT_METHOD_SERVICE) as InputMethodManager

            val message: TextView = findViewById(R.id.message)
            micButton = findViewById(R.id.mic_button)
            val backButton: ImageButton = findViewById(R.id.back_button)

            val lastIMELanguage = inputMethodManager.lastInputMethodSubtype.languageTag.ifEmpty { inputMethodManager.lastInputMethodSubtype.locale }


            message.text = "last IME language: $lastIMELanguage using it for speech recognition"

            micButton.setOnClickListener {
                Log.i("AA", "mic click!")

                val audioIntent = Intent(RecognizerIntent.ACTION_RECOGNIZE_SPEECH)
                audioIntent.putExtra(RecognizerIntent.EXTRA_PARTIAL_RESULTS, true)
                //TODO: check if language is supported?
                audioIntent.putExtra(RecognizerIntent.EXTRA_LANGUAGE, lastIMELanguage)

                s.startListening(audioIntent)
                micButton.setColorFilter(Color.parseColor("#FF0000"))
            }


            backButton.setOnClickListener {
                Log.i("AA", "back click!")

                val switched = if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.P) {
                    switchToPreviousInputMethod()
                } else {
                    inputMethodManager.switchToLastInputMethod(window?.window?.attributes?.token)
                }

                if (!switched) {
                    Log.i("AA", "last ime didn't work, let's show the switch!")
                    inputMethodManager.showInputMethodPicker()
                }
            }

            val spaceButton: Button = findViewById(R.id.space)
            spaceButton.setOnClickListener {
                currentInputConnection.also { ic: InputConnection ->
                    ic.commitText(" ", 1)
                }
            }

            val commaButton: Button = findViewById(R.id.comma)
            commaButton.setOnClickListener {
                currentInputConnection.also { ic: InputConnection ->
                    ic.commitText(",", 1)
                }
            }

            val fullstopButton: Button = findViewById(R.id.fullstop)
            fullstopButton.setOnClickListener {
                currentInputConnection.also { ic: InputConnection ->
                    ic.commitText(".", 1)
                }
            }

            val backspaceButton: ImageButton = findViewById(R.id.backspace)
            backspaceButton.setOnClickListener {
                currentInputConnection.also { ic: InputConnection ->
                    ic.deleteSurroundingText(1, 0)
                }
            }
        }
    }


    //we do not support suggestions
    override fun onCreateCandidatesView(): View? {
        return null
    }

    override fun onStartInputView(info: EditorInfo?, restarting: Boolean) {
        super.onStartInputView(info, restarting)
        Log.i("AA", "onStartInputView: " + info?.inputType + "/" + info?.imeOptions + "/" + restarting)


        currentInputConnection.also {
            val listener = SpeechRecognitionListener(it)
            s.setRecognitionListener(listener)
        }
    }


    internal inner class SpeechRecognitionListener(private val currentInputConnection: InputConnection) : RecognitionListener {

        override fun onReadyForSpeech(p0: Bundle?) {
            Log.i("AA", "IME OnReadyForSpeech")
            micButton.setColorFilter(Color.parseColor("#00FF00"))
        }


        override fun onBeginningOfSpeech() {
            Log.i("AA", "IME onBeginningOfSpeech")
        }


        override fun onRmsChanged(p0: Float) {
            TODO("Not yet implemented")
        }

        override fun onBufferReceived(p0: ByteArray?) {
            TODO("Not yet implemented")
        }

        override fun onEndOfSpeech() {
            Log.i("AA", "IME onEndOfSpeech")

        }

        override fun onError(p0: Int) {
            TODO("Not yet implemented")
        }

        override fun onResults(p0: Bundle?) {
            Log.i("AA", "IME onResults")

            currentInputConnection.also { ic: InputConnection ->
                ic.commitText(p0?.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)?.get(0), 1)
            }

            micButton.setColorFilter(Color.parseColor("#FFFFFF"))
        }

        override fun onPartialResults(p0: Bundle?) {
            Log.i("AA", "IME onPartialResults")

            currentInputConnection.also { ic: InputConnection ->
                ic.setComposingText(p0?.getStringArrayList(SpeechRecognizer.RESULTS_RECOGNITION)?.get(0), 1)
            }
        }

        override fun onEvent(p0: Int, p1: Bundle?) {
            TODO("Not yet implemented")
        }

    }

}


