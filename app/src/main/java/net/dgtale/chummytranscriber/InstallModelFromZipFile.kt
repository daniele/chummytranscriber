package net.dgtale.chummytranscriber

import android.content.ContentResolver
import android.net.Uri
import android.os.Bundle
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.GravityCompat
import androidx.core.view.children
import kotlinx.coroutines.*
import java.io.File
import java.io.FileOutputStream
import java.util.zip.ZipEntry
import java.util.zip.ZipInputStream


class InstallModelFromZipFile : AppCompatActivity() {
    private val BUFFER_SIZE = 8192//2048;
    private val install = SupervisorJob()
    private val scope = CoroutineScope(Dispatchers.IO + install)


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_install_model_from_zip_file)
        setSupportActionBar(findViewById(R.id.toolbar))

        val layout :LinearLayout = findViewById(R.id.installprogresslayout)

        val data: Uri? = this.intent.data
        if (data != null) {
            val cr :ContentResolver = this.contentResolver
            val buffer = ByteArray(BUFFER_SIZE)
            var count: Int

            scope.launch {
                ZipInputStream(cr.openInputStream(data)).use { zis ->
                    var ze: ZipEntry?
                    while (zis.nextEntry.also { ze = it } != null) {
                        val fileName = ze!!.name
                        withContext(Dispatchers.Main) {
                            layout.addView(addProgress("Unzipping file $fileName"))

                        }
                        val file = File(getExternalFilesDir(null)?.absolutePath, fileName)
                        val dir = if (ze!!.isDirectory) file else file.getParentFile()

                        if (!dir.isDirectory)
                            dir.mkdirs()
                        if (ze!!.isDirectory) continue
                        FileOutputStream(file).use {
                            while (zis.read(buffer).also { count = it } != -1)
                                it.write(buffer, 0, count)
                        }
                    }
                }

                withContext(Dispatchers.Main) {
                       // layout.removeAllViews()
                    layout.addView(addProgress("ALL DONE!"))

                }
            }
        }

    }

    private fun addProgress(text: String?): TextView {
        val textView = TextView(this)
        textView.text = text
        val params = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        )
        textView.layoutParams = params
        textView.textSize = 24F
        return textView
    }

}