package net.dgtale.chummytranscriber

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Context.MODE_PRIVATE
import android.content.Intent
import android.speech.RecognizerIntent
import androidx.core.os.bundleOf
import java.io.File


class SupportedLanguagesReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent) {

        val localeArray = ArrayList<String>()

        context.getSharedPreferences("chummytranscriber", MODE_PRIVATE).all.forEach {
            if(File(context.getExternalFilesDir(null)?.absolutePath + "/" + it.key).isDirectory)
                localeArray.add(it.value.toString())
        }

        setResultExtras(bundleOf
        (RecognizerIntent.EXTRA_SUPPORTED_LANGUAGES to localeArray )
        )

    }
}