package net.dgtale.chummytranscriber

import android.Manifest.permission.RECORD_AUDIO
import android.app.Activity
import android.content.Context
import android.os.Bundle
import android.os.LocaleList
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.appcompat.app.AppCompatActivity
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import java.io.File
import kotlin.collections.ArrayList

class SetupActivity() : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val path = getExternalFilesDir(null)


        setContentView(R.layout.activity_setup)
        setSupportActionBar(findViewById(R.id.toolbar))

        requestPermissions(arrayOf(RECORD_AUDIO), 1)


        val instructions: TextView = findViewById(R.id.instructions)
        instructions.text = "Place the models in directory: ${path?.absolutePath}. The available directories will be shown below."

        instructions.linksClickable = true

        listAvailableModels(path)

    }

    private fun listAvailableModels(path: File?) {
        val models: MutableList<String> = ArrayList()
        val adapter = ModelsAdapter(this, models)

        val listView: ListView = findViewById(R.id.avail_models)
        listView.adapter = adapter

        with(path?.listFiles()?.filter { it.isDirectory }) {
            this?.forEach { models.add(it.name) }
        }

    }

class ModelsAdapter(private val context: Activity, private val modelPaths: List<String>)
    : ArrayAdapter<String>(context, R.layout.model_item, modelPaths) {

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        val inflater = context.layoutInflater
        val row = inflater.inflate(R.layout.model_item, null, true)
        val prefs = context.getSharedPreferences("chummytranscriber", MODE_PRIVATE)

        val modelPathTextView = row.findViewById<TextView>(R.id.textView)
        val modelPath = modelPaths[position]

        modelPathTextView.text = modelPath

        val modelLangToSearch = prefs.getString(modelPath, "")
        var modelLangPosition = -1

        //get list of enabled locales
        val localeArray = ArrayList<String>()
        val localeMap = HashMap<String, String>()
        val locales: LocaleList = LocaleList.getDefault()
        for (i in 0 until locales.size()) {
            localeMap[locales[i].displayLanguage] = locales[i].language
            localeArray.add(locales[i].displayLanguage)
            if(locales[i].language == modelLangToSearch)
                modelLangPosition = i
        }


        val spinner = row.findViewById<Spinner>(R.id.spinner)
        spinner.adapter = ArrayAdapter(context, android.R.layout.simple_spinner_item, localeArray)

        spinner.setSelection(modelLangPosition)

        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
            override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
                val chosenModelLang = parent?.getItemAtPosition(position).toString()
                with(prefs.edit()) {
                    putString(modelPath, localeMap[chosenModelLang])
                    apply()
                }
            }

            override fun onNothingSelected(parent: AdapterView<*>?) {
                TODO("Not yet implemented")
            }


        }
        return row
    }


    }
}